# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#include <tunables/global>

@prefix@/** {
  #include <abstractions/apertis-device-enumeration>
  #include <abstractions/chaiwala-base>
  #include <abstractions/dbus-strict>
  #include <abstractions/dbus-session-strict>
  #include <abstractions/fonts>

  @prefix@/{bin,libexec}/* pix,
  @prefix@/{bin,lib,libexec}/{,**} mr,
  @prefix@/share/{,**} r,

  owner /var/Applications/@BUNDLE_ID@/users/** rwk,
  owner @{PROC}/@{pid}/** r,

  # WebKit starts subprocesses to do networking and web rendering.
  # Run them with the same privileges as the rest of our webapp.
  /usr/lib/*/webkit*/WebKitNetworkProcess ix,
  /usr/lib/*/webkit*/WebKitWebProcess ix,
  owner /dev/shm/WK2SharedMemory.* rw,

  owner link
        subset /var/Applications/@BUNDLE_ID@/users/**
            -> /var/Applications/@BUNDLE_ID@/users/**,

  dbus send
       bus=session
       path=/org/freedesktop/DBus
       interface=org.freedesktop.DBus
       member={RequestName,ReleaseName}
       peer=(name=org.freedesktop.DBus),

  dbus send
       bus=session
       interface=org.gtk.vfs.MountTracker
       member=ListMountableInfo
       peer=(label=unconfined),

  dbus bind
       bus=session name="@BUNDLE_ID@",

  dbus (send, receive)
       bus=session peer=(label=/Applications/@BUNDLE_ID@/**),

  signal receive set=(kill) peer=/usr/bin/canterbury,
}
